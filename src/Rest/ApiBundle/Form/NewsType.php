<?php

namespace Rest\ApiBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class NewsType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title','voryx_entity',['class' => 'Rest\ApiBundle\Entity\News'])
            ->add('text','voryx_entity',['class' => 'Rest\ApiBundle\Entity\News']);
            //->add('updateTime', 'datetime')
            //->add('addTime', 'datetime');
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class'      => 'Rest\ApiBundle\Entity\News',
            'csrf_protection' => false,
        ));
    }

}
